#pragma once
#include "Horario.h"
#include "Autos.h"


namespace Estacionamiento {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;

	/// <summary>
	/// Resumen de Admin
	/// </summary>
	public ref class Admin : public System::Windows::Forms::Form
	{
	public:
		Admin(void)
		{
			InitializeComponent();
			listar();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Admin()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:




	private: System::Windows::Forms::Button^  button1;


	public: String ^ datosConexion = "datasource=localhost;port=3306;username=root;password=2915;database=estacionamiento";

	public:






	private:



	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  tabPage5;
	private: System::Windows::Forms::DateTimePicker^  dateTimePicker2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::ComboBox^  comboBox4;
	private: System::Windows::Forms::ComboBox^  comboBox3;
	private: System::Windows::Forms::TextBox^  textBox15;
	public: System::Windows::Forms::TextBox^  textBox14;
	private:
	private: System::Windows::Forms::TextBox^  textBox12;
	public:
	private: System::Windows::Forms::TextBox^  textBox11;






	private: System::Windows::Forms::DataGridView^  dataGridView3;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;








	private:


	private:




	public:




	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Admin::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->dateTimePicker2 = (gcnew System::Windows::Forms::DateTimePicker());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->dataGridView3 = (gcnew System::Windows::Forms::DataGridView());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->tabControl1->SuspendLayout();
			this->tabPage5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView3))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button1->Location = System::Drawing::Point(744, 581);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 55);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Salir";
			this->button1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Admin::button1_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage5);
			this->tabControl1->Location = System::Drawing::Point(12, 12);
			this->tabControl1->Multiline = true;
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(822, 563);
			this->tabControl1->TabIndex = 1;
			// 
			// tabPage5
			// 
			this->tabPage5->Controls->Add(this->label7);
			this->tabPage5->Controls->Add(this->label6);
			this->tabPage5->Controls->Add(this->label5);
			this->tabPage5->Controls->Add(this->label4);
			this->tabPage5->Controls->Add(this->label3);
			this->tabPage5->Controls->Add(this->label2);
			this->tabPage5->Controls->Add(this->label1);
			this->tabPage5->Controls->Add(this->dateTimePicker2);
			this->tabPage5->Controls->Add(this->button3);
			this->tabPage5->Controls->Add(this->button10);
			this->tabPage5->Controls->Add(this->button9);
			this->tabPage5->Controls->Add(this->comboBox4);
			this->tabPage5->Controls->Add(this->comboBox3);
			this->tabPage5->Controls->Add(this->textBox15);
			this->tabPage5->Controls->Add(this->textBox14);
			this->tabPage5->Controls->Add(this->textBox12);
			this->tabPage5->Controls->Add(this->textBox11);
			this->tabPage5->Controls->Add(this->dataGridView3);
			this->tabPage5->Controls->Add(this->label14);
			this->tabPage5->Location = System::Drawing::Point(4, 22);
			this->tabPage5->Name = L"tabPage5";
			this->tabPage5->Padding = System::Windows::Forms::Padding(3);
			this->tabPage5->Size = System::Drawing::Size(814, 537);
			this->tabPage5->TabIndex = 5;
			this->tabPage5->Text = L"Mas Opciones";
			this->tabPage5->UseVisualStyleBackColor = true;
			this->tabPage5->Click += gcnew System::EventHandler(this, &Admin::tabPage5_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(459, 420);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(92, 20);
			this->label7->TabIndex = 20;
			this->label7->Text = L"Contrase�a";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(20, 420);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(120, 20);
			this->label6->TabIndex = 19;
			this->label6->Text = L"Tipo de Usuario";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(459, 377);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(65, 20);
			this->label5->TabIndex = 18;
			this->label5->Text = L"Nombre";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(20, 374);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(99, 20);
			this->label4->TabIndex = 17;
			this->label4->Text = L"Especialidad";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(459, 333);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(54, 20);
			this->label3->TabIndex = 16;
			this->label3->Text = L"Fecha";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(20, 335);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(108, 20);
			this->label2->TabIndex = 15;
			this->label2->Text = L"Id. de Usuario";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(6, 21);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(128, 20);
			this->label1->TabIndex = 14;
			this->label1->Text = L"Ingresar Nombre";
			// 
			// dateTimePicker2
			// 
			this->dateTimePicker2->Enabled = false;
			this->dateTimePicker2->Location = System::Drawing::Point(557, 335);
			this->dateTimePicker2->MaxDate = System::DateTime(2019, 12, 31, 0, 0, 0, 0);
			this->dateTimePicker2->MinDate = System::DateTime(2000, 1, 1, 0, 0, 0, 0);
			this->dateTimePicker2->Name = L"dateTimePicker2";
			this->dateTimePicker2->Size = System::Drawing::Size(210, 20);
			this->dateTimePicker2->TabIndex = 13;
			this->dateTimePicker2->Value = System::DateTime(2018, 4, 25, 4, 37, 10, 588);
			// 
			// button3
			// 
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
			this->button3->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button3->Location = System::Drawing::Point(24, 474);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 55);
			this->button3->TabIndex = 12;
			this->button3->Text = L"Agregar";
			this->button3->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Admin::button3_Click_1);
			// 
			// button10
			// 
			this->button10->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button10.Image")));
			this->button10->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button10->Location = System::Drawing::Point(728, 474);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(75, 55);
			this->button10->TabIndex = 10;
			this->button10->Text = L"Actualizar";
			this->button10->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &Admin::button10_Click);
			// 
			// button9
			// 
			this->button9->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button9.Image")));
			this->button9->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button9->Location = System::Drawing::Point(375, 474);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(75, 55);
			this->button9->TabIndex = 9;
			this->button9->Text = L"Baja";
			this->button9->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &Admin::button9_Click);
			// 
			// comboBox4
			// 
			this->comboBox4->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox4->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"General", L"Administrador" });
			this->comboBox4->Location = System::Drawing::Point(150, 419);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(210, 21);
			this->comboBox4->TabIndex = 7;
			this->comboBox4->SelectedIndexChanged += gcnew System::EventHandler(this, &Admin::comboBox4_SelectedIndexChanged);
			this->comboBox4->SelectionChangeCommitted += gcnew System::EventHandler(this, &Admin::comboBox4_SelectionChangeCommitted);
			this->comboBox4->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Admin::comboBox4_KeyPress);
			// 
			// comboBox3
			// 
			this->comboBox3->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox3->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Items->AddRange(gcnew cli::array< System::Object^  >(11) {
				L"Ingenieria Electr�nica", L"Ingenieria El�ctrica",
					L"Ingenieria Civil", L"Ingenieria Mec�nica", L"Ingenieria Industrial", L"Ingenieria Qu�mica", L"Ingenieria en Gesti�n Empresarial",
					L"Ingenieria en Sist. Computacionales", L"Licenciatura en Administraci�n", L"Ciencias Basicas", L"Administrativos"
			});
			this->comboBox3->Location = System::Drawing::Point(150, 376);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(210, 21);
			this->comboBox3->TabIndex = 5;
			// 
			// textBox15
			// 
			this->textBox15->Location = System::Drawing::Point(557, 379);
			this->textBox15->MaxLength = 30;
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(210, 20);
			this->textBox15->TabIndex = 6;
			// 
			// textBox14
			// 
			this->textBox14->Location = System::Drawing::Point(557, 420);
			this->textBox14->MaxLength = 4;
			this->textBox14->Name = L"textBox14";
			this->textBox14->PasswordChar = '*';
			this->textBox14->Size = System::Drawing::Size(210, 20);
			this->textBox14->TabIndex = 8;
			this->textBox14->Tag = L"";
			this->textBox14->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Admin::textBox14_KeyPress);
			// 
			// textBox12
			// 
			this->textBox12->Enabled = false;
			this->textBox12->Location = System::Drawing::Point(150, 333);
			this->textBox12->MaxLength = 5;
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(210, 20);
			this->textBox12->TabIndex = 3;
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(146, 21);
			this->textBox11->MaxLength = 15;
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(247, 20);
			this->textBox11->TabIndex = 0;
			this->textBox11->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Admin::textBox11_KeyPress);
			// 
			// dataGridView3
			// 
			this->dataGridView3->AllowUserToAddRows = false;
			this->dataGridView3->AllowUserToDeleteRows = false;
			this->dataGridView3->AllowUserToOrderColumns = true;
			this->dataGridView3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView3->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->dataGridView3->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
			this->dataGridView3->BackgroundColor = System::Drawing::SystemColors::Window;
			this->dataGridView3->ColumnHeadersHeight = 24;
			this->dataGridView3->Location = System::Drawing::Point(6, 47);
			this->dataGridView3->Name = L"dataGridView3";
			this->dataGridView3->ReadOnly = true;
			this->dataGridView3->RowHeadersVisible = false;
			this->dataGridView3->RowHeadersWidth = 80;
			this->dataGridView3->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView3->Size = System::Drawing::Size(797, 228);
			this->dataGridView3->TabIndex = 2;
			this->dataGridView3->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Admin::dataGridView3_CellClick_1);
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label14->Location = System::Drawing::Point(155, 21);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(0, 20);
			this->label14->TabIndex = 0;
			// 
			// Admin
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(844, 639);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->tabControl1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(860, 678);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(860, 678);
			this->Name = L"Admin";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Administrador";
			this->Load += gcnew System::EventHandler(this, &Admin::Admin_Load);
			this->tabControl1->ResumeLayout(false);
			this->tabPage5->ResumeLayout(false);
			this->tabPage5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView3))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Admin_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void tabPage2_Click(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	Close();
	


}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	
}

		 

private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {

}
private: System::Void dataGridView3_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
private: System::Void dataGridView3_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
}
private: System::Void dataGridView3_CellClick_1(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	//label15->Text = dataGridView3->CurrentRow->Cells[0]->Value->ToString(); // regresa la primera columna de la tabla
//label15->Text = dataGridView3->CurrentCell->Value->ToString();

	if (textBox11->Text == "") {

	}
	else
	{
		textBox12->Text = dataGridView3->CurrentRow->Cells[0]->Value->ToString();
		dateTimePicker2->Text = dataGridView3->CurrentRow->Cells[1]->Value->ToString();
		comboBox3->Text = dataGridView3->CurrentRow->Cells[2]->Value->ToString();
		textBox15->Text = dataGridView3->CurrentRow->Cells[3]->Value->ToString();
		comboBox4->Text = dataGridView3->CurrentRow->Cells[4]->Value->ToString();
		textBox14->Text = dataGridView3->CurrentRow->Cells[5]->Value->ToString();
	}
	
}
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
	//borrar_usuario
	MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
	String^ consulta = "call borrar_usuario('" + textBox12->Text + "')";


	if (textBox12->Text != "") {
		//Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
		try {
			conexion->Open();
			MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
			MySqlDataReader^ lector;

			try {
				lector = comando->ExecuteReader();
				textBox12->Clear();
				//textBox13->Clear();
				comboBox3->Text = "";
				textBox15->Clear();
				comboBox4->Text = "";
				textBox14->Clear();
				dataGridView3->Rows->RemoveAt(dataGridView3->CurrentRow->Index);

				MessageBox::Show("Usuario dado de baja", "Baja",
					MessageBoxButtons::OK, MessageBoxIcon::Hand);
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}
		}
		catch (Exception^ex) {
			MessageBox::Show(ex->Message);
		}

	}
}
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
	//borrar_usuario
	MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
	String^ consulta = "call actualizar_usuario(" + textBox12->Text + ",'" + textBox15->Text + "','" + comboBox3->Text + "','" + comboBox4->Text + "','" + textBox14->Text + "')";


	if (textBox11->Text != "") {
		//Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
		try {
			conexion->Open();
			MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
			MySqlDataReader^ lector;

			try {
				lector = comando->ExecuteReader();
				MessageBox::Show("Datos modificados", "Act",
					MessageBoxButtons::OK, MessageBoxIcon::Asterisk);
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}


		}
		catch (Exception^ex) {
			MessageBox::Show(ex->Message);
		}

	}

}


private: System::Void tabPage5_Click(System::Object^  sender, System::EventArgs^  e) {
}


private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

}
private: System::Void textBox11_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (textBox11->Text != "") {
		button9->Enabled = true;
		button10->Enabled = true;
		button3->Enabled = false;
		MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
		String^ consulta = "call buscar_usuarios_coincidencia('" + textBox11->Text + "')";

		DataTable^ tabla;//crea una tabla donde se agregaran los resultados
						 //Valido que todos los campos del formulario esten rellenos y no esten vacios
		if (textBox11->Text != "") {
			//Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
			try {
				conexion->Open();
				MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);	//guarda los resultados del procedimineto			   

				try {
					tabla = gcnew DataTable();
					resultados->Fill(tabla);
					dataGridView3->DataSource = tabla;// guarda en la propiedad de data grid view1, data source laos resultados de la tabla
				}
				catch (Exception^ex) {
					MessageBox::Show(ex->Message);
				}
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}

		}
	}
	else {
		button3->Enabled = true;
		button9->Enabled = false;
		button10->Enabled = false;
		dataGridView3->ClearSelection();
		//listar();
		textBox12->Clear();
		comboBox3->Text = "";
		textBox15->Clear();
		comboBox4->Text = "";
		textBox14->Clear();
		this->dateTimePicker2->Value = System::DateTime::Now;
	}

	
}

		 void listar() {

			 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
			 String^ consulta = "call todos()";

			 DataTable^ tabla;//crea una tabla donde se agregaran los resultados
							  //Valido que todos los campos del formulario esten rellenos y no esten vacios
				 //Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
				 try {
					 conexion->Open();
					 MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);	//guarda los resultados del procedimineto			   

					 try {
						 tabla = gcnew DataTable();
						 resultados->Fill(tabla);
						 dataGridView3->DataSource = tabla;// guarda en la propiedad de data grid view1, data source laos resultados de la tabla
					 }
					 catch (Exception^ex) {
						 MessageBox::Show(ex->Message);
					 }
				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
				 }

		 }

private: System::Void button3_Click_1(System::Object^  sender, System::EventArgs^  e) {
	MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
	String^ consulta = "call nuevo_usuario('" + textBox15->Text + "', '" + comboBox3->Text + "', '" + comboBox4->Text + "', '" + textBox14->Text + "', '" + dateTimePicker2->Value.ToString("yyyy-MM-dd") + "')";



	//Valido que todos los campos del formulario esten rellenos y no esten vacios
	if (textBox15->Text != "" && comboBox3->Text != "" && comboBox4->Text != "") {

		//If donde valido segun mi criterio lo de el tipo de usuario el cual debe tener contrasena si es administrador de lo
		//contrario si es general no debe de tener una contrasena
		if (comboBox4->Text == "General" && textBox14->Text == "" || comboBox4->Text == "Administrador" && textBox14->Text != "") {

			//Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
			try {
				conexion->Open();
				MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);

				MySqlDataReader^ lector;
				try {
					int x;
					lector = comando->ExecuteReader();
					while (lector->Read())
						x = lector->GetInt16(0);

					Horario^ xh = gcnew Horario(textBox15->Text);
					Autos^ aut = gcnew Autos(); //FALTA PASAR ID CHECAR UN PROC
					//	x->Show();
					this->Hide();
					//this->Hide(); //OCULTA ESTE FORM
					aut->ShowDialog();
					xh->ShowDialog(); //APARECE EL FORM DE MENU
					

					this->Show(); //AL CERRAR EL MENU SE VUELVE A EJECUTAR LA CONTINUACION DE ESTE CODIGO CON LO CUAL LO VOLVEMOS A MOSTRAR

								  //Borrar los datos de los texbox
					textBox14->Clear();
					textBox15->Clear();


				}
				catch (Exception^ex) {
					MessageBox::Show(ex->Message);
				}
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}


		}
		else {
			MessageBox::Show("Un usuario general no tiene contrasena \n" + " Un usuario Administrador debe tener contrasena", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
	}
	else {
		MessageBox::Show("Rellene todos los campos por favor", "ERROR",
			MessageBoxButtons::OK, MessageBoxIcon::Error);
	}

}
private: System::Void comboBox4_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	if (comboBox4->Text == "General")
		textBox14->Enabled=false;
	else
		textBox14->Enabled=true;
}
private: System::Void textBox14_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {

	if (!Char::IsDigit(e->KeyChar) && e->KeyChar != 0x08) {
		e->Handled = true;


	}
}
private: System::Void comboBox4_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void comboBox4_SelectionChangeCommitted(System::Object^  sender, System::EventArgs^  e) {
	if (comboBox4->Text == "General")
		textBox14->Enabled = true;
	else
		textBox14->Enabled = false;
}
};
}
