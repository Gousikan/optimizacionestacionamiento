#pragma once
#include <iostream>
#include <cstdlib>
namespace Estacionamiento {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;

	/// <summary>
	/// Resumen de Horario
	/// </summary>
	public ref class Horario : public System::Windows::Forms::Form
	{
	 String^ nombre;
	 String^ iddelusuario;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::ComboBox^  comboBox3;
	private: System::Windows::Forms::ComboBox^  comboBox4;
	private: System::Windows::Forms::ComboBox^  comboBox5;
	private: System::Windows::Forms::ComboBox^  comboBox6;
	private: System::Windows::Forms::ComboBox^  comboBox7;
	private: System::Windows::Forms::ComboBox^  comboBox8;
	private: System::Windows::Forms::ComboBox^  comboBox9;
	private: System::Windows::Forms::ComboBox^  comboBox10;
	public: String ^ ejemplo;

	public:
		Horario(String^ nom)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
			nombre = nom;
			this->textbox->Text = nombre;
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Horario()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	public: String ^ datosConexion = "datasource=localhost;port=3306;username=root;password=1234;database=estacionamiento";
	private: System::Windows::Forms::TextBox^  textbox;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;



	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Horario::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textbox = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox7 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox8 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox9 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox10 = (gcnew System::Windows::Forms::ComboBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(120, 22);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(57, 16);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Nombre";
			// 
			// textbox
			// 
			this->textbox->Enabled = false;
			this->textbox->Location = System::Drawing::Point(183, 21);
			this->textbox->Name = L"textbox";
			this->textbox->Size = System::Drawing::Size(199, 20);
			this->textbox->TabIndex = 1;
			// 
			// button1
			// 
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button1->Location = System::Drawing::Point(437, 244);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 51);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Agregar";
			this->button1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Horario::button1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(54, 70);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(33, 20);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Dia";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(51, 101);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(53, 20);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Lunes";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(51, 127);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(58, 20);
			this->label4->TabIndex = 5;
			this->label4->Text = L"Martes";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(51, 153);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(76, 20);
			this->label5->TabIndex = 6;
			this->label5->Text = L"Miercoles";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(52, 179);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(59, 20);
			this->label6->TabIndex = 7;
			this->label6->Text = L"Jueves";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(52, 205);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(63, 20);
			this->label7->TabIndex = 8;
			this->label7->Text = L"Viernes";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label8->Location = System::Drawing::Point(144, 67);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(125, 20);
			this->label8->TabIndex = 9;
			this->label8->Text = L"Hora de entrada";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label9->Location = System::Drawing::Point(345, 67);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(111, 20);
			this->label9->TabIndex = 10;
			this->label9->Text = L"Hora de salida";
			// 
			// comboBox1
			// 
			this->comboBox1->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox1->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox1->Location = System::Drawing::Point(145, 93);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(121, 21);
			this->comboBox1->TabIndex = 11;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Horario::comboBox1_SelectedIndexChanged);
			// 
			// comboBox2
			// 
			this->comboBox2->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox2->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox2->Location = System::Drawing::Point(145, 120);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(121, 21);
			this->comboBox2->TabIndex = 12;
			// 
			// comboBox3
			// 
			this->comboBox3->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox3->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox3->Location = System::Drawing::Point(145, 147);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(121, 21);
			this->comboBox3->TabIndex = 13;
			// 
			// comboBox4
			// 
			this->comboBox4->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox4->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox4->Location = System::Drawing::Point(145, 176);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(121, 21);
			this->comboBox4->TabIndex = 14;
			// 
			// comboBox5
			// 
			this->comboBox5->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox5->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox5->Location = System::Drawing::Point(145, 203);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(121, 21);
			this->comboBox5->TabIndex = 15;
			// 
			// comboBox6
			// 
			this->comboBox6->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox6->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox6->Location = System::Drawing::Point(342, 93);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(121, 21);
			this->comboBox6->TabIndex = 16;
			// 
			// comboBox7
			// 
			this->comboBox7->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox7->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox7->FormattingEnabled = true;
			this->comboBox7->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox7->Location = System::Drawing::Point(342, 120);
			this->comboBox7->Name = L"comboBox7";
			this->comboBox7->Size = System::Drawing::Size(121, 21);
			this->comboBox7->TabIndex = 17;
			// 
			// comboBox8
			// 
			this->comboBox8->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox8->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox8->FormattingEnabled = true;
			this->comboBox8->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox8->Location = System::Drawing::Point(342, 147);
			this->comboBox8->Name = L"comboBox8";
			this->comboBox8->Size = System::Drawing::Size(121, 21);
			this->comboBox8->TabIndex = 18;
			// 
			// comboBox9
			// 
			this->comboBox9->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox9->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox9->FormattingEnabled = true;
			this->comboBox9->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox9->Location = System::Drawing::Point(342, 174);
			this->comboBox9->Name = L"comboBox9";
			this->comboBox9->Size = System::Drawing::Size(121, 21);
			this->comboBox9->TabIndex = 19;
			// 
			// comboBox10
			// 
			this->comboBox10->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBox10->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::ListItems;
			this->comboBox10->FormattingEnabled = true;
			this->comboBox10->Items->AddRange(gcnew cli::array< System::Object^  >(15) {
				L"", L"7", L"8", L"9", L"10", L"11", L"12", L"13",
					L"14", L"15", L"16", L"17", L"18", L"19", L"20"
			});
			this->comboBox10->Location = System::Drawing::Point(342, 201);
			this->comboBox10->Name = L"comboBox10";
			this->comboBox10->Size = System::Drawing::Size(121, 21);
			this->comboBox10->TabIndex = 20;
			// 
			// Horario
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(534, 319);
			this->Controls->Add(this->comboBox10);
			this->Controls->Add(this->comboBox9);
			this->Controls->Add(this->comboBox8);
			this->Controls->Add(this->comboBox7);
			this->Controls->Add(this->comboBox6);
			this->Controls->Add(this->comboBox5);
			this->Controls->Add(this->comboBox4);
			this->Controls->Add(this->comboBox3);
			this->Controls->Add(this->comboBox2);
			this->Controls->Add(this->comboBox1);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textbox);
			this->Controls->Add(this->label1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximumSize = System::Drawing::Size(550, 358);
			this->MinimumSize = System::Drawing::Size(550, 358);
			this->Name = L"Horario";
			this->Text = L"Horario";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	 
		int contador = 0;
		int estado1 = 0;
		int estado2 = 0;
		int estado3 = 0;
		int	estado4 = 0;
		int estado5 = 0;
		int c1 = comboBox1->SelectedIndex;
		int c2 = comboBox2->SelectedIndex;
		int c3 = comboBox3->SelectedIndex;
		int c4 = comboBox4->SelectedIndex;
		int c5 = comboBox5->SelectedIndex;
		int c6 = comboBox6->SelectedIndex;
		int c7 = comboBox7->SelectedIndex;
		int c8 = comboBox8->SelectedIndex;
		int c9 = comboBox9->SelectedIndex;
		int c10 = comboBox10->SelectedIndex;

		
		if (comboBox1->Text == "" && comboBox6->Text == "" || comboBox1->Text != "" && comboBox6->Text == "" || comboBox1->Text == "" && comboBox6->Text != "" || c1 >= c6 ) {
			contador =contador + 1;
			estado1 = 1;
		}

		if (comboBox2->Text == "" && comboBox7->Text == "" || comboBox2->Text != "" && comboBox7->Text == "" || comboBox2->Text == "" && comboBox7->Text != "" || c2 >= c7) {
			contador = contador + 1;
			estado2 = 1;
		}

		if (comboBox3->Text == "" && comboBox8->Text == ""|| comboBox3->Text != "" && comboBox8->Text == "" || comboBox3->Text == "" && comboBox8->Text != "" || c3 >= c8) {
			contador = contador + 1;
			estado3 = 1;
		}

		if (comboBox4->Text == "" && comboBox9->Text == "" || comboBox4->Text != "" && comboBox9->Text == "" || comboBox4->Text == "" && comboBox9->Text != "" || c4 >= c9) {
			contador = contador + 1;
			estado4 = 1;
		}

		if (comboBox5->Text == "" && comboBox10->Text == "" || comboBox5->Text != "" && comboBox10->Text == "" || comboBox5->Text == "" && comboBox10->Text != "" || c5 >= c10) {
			contador = contador + 1;
			estado5 = 1;
		}
		


		if (contador <= 4) {
			buscarid();
			

			if (estado1 == 0) {
				agregar1();
			}
			if (estado2 == 0) {
				agregar2();
			}
			if (estado3 == 0) {
				agregar3();
			}
			if (estado4 == 0) {
				agregar4();
			}
			if (estado5 == 0) {
				agregar5();
			}
			
			MessageBox::Show("Guardado con exito", "Exito",
				MessageBoxButtons::OK, MessageBoxIcon::Asterisk);
			Close();

		}
		else
		{
			MessageBox::Show("Falta rellenar datos", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
		

	}
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	}
			

			 public:
				 void buscarid() {
					 //borrar_usuario
					 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
					 String^ consulta = "call buscarid()";

					 try {
						 conexion->Open();
						 MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
						 MySqlDataReader^ lector;

						 try {
							 lector = comando->ExecuteReader();
							 lector->Read();
							 iddelusuario = "" + lector[0];
							 ;
						 }
						 catch (Exception^ex) {
							 MessageBox::Show(ex->Message);
						 }
					 }
					 catch (Exception^ex) {
						 MessageBox::Show(ex->Message);
					 }

				 }



		public:
		 void agregar1() {

			 //borrar_usuario
			 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
			 String^ consulta = "call nuevo_horario('" + label3->Text + "', '" + comboBox1->Text + "','" + comboBox6->Text + "', " + iddelusuario + ")";

			 try {
				 conexion->Open();
				 MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
				 MySqlDataReader^ lector;

				 try {
					 lector = comando->ExecuteReader();

					 ;
				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
				 }
			 }
			 catch (Exception^ex) {
				 MessageBox::Show(ex->Message);
			 }

	}

		 void agregar2() {

			 //borrar_usuario
			 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
			 String^ consulta = "call nuevo_horario('" + label4->Text + "', '" + comboBox2->Text + "','" + comboBox7->Text + "', " + iddelusuario + ")";

			 try {
				 conexion->Open();
				 MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
				 MySqlDataReader^ lector;

				 try {
					 lector = comando->ExecuteReader();

					 ;
				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
				 }
			 }
			 catch (Exception^ex) {
				 MessageBox::Show(ex->Message);
			 }

		 }

		 void agregar3() {

			 //borrar_usuario
			 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
			 String^ consulta = "call nuevo_horario('" + label5->Text + "', '" + comboBox3->Text + "','" + comboBox8->Text + "', " + iddelusuario + ")";

			 try {
				 conexion->Open();
				 MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
				 MySqlDataReader^ lector;

				 try {
					 lector = comando->ExecuteReader();

					 ;
				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
				 }
			 }
			 catch (Exception^ex) {
				 MessageBox::Show(ex->Message);
			 }

		 }

		 void agregar4() {

			 //borrar_usuario
			 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
			 String^ consulta = "call nuevo_horario('" + label6->Text + "', '" + comboBox4->Text + "','" + comboBox9->Text + "', " + iddelusuario + ")";

			 try {
				 conexion->Open();
				 MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
				 MySqlDataReader^ lector;

				 try {
					 lector = comando->ExecuteReader();

					 ;
				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
				 }
			 }
			 catch (Exception^ex) {
				 MessageBox::Show(ex->Message);
			 }

		 }

		 void agregar5() {

			 //borrar_usuario
			 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
			 String^ consulta = "call nuevo_horario('" + label7->Text + "', '" + comboBox5->Text + "','" + comboBox10->Text + "', " + iddelusuario + ")";

			 try {
				 conexion->Open();
				 MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
				 MySqlDataReader^ lector;

				 try {
					 lector = comando->ExecuteReader();

					 ;
				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
				 }
			 }
			 catch (Exception^ex) {
				 MessageBox::Show(ex->Message);
			 }

		 }
};
}