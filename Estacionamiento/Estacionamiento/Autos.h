
#pragma once
#using <System.DLL>
#using <System.Drawing.DLL>
#using <System.Windows.Forms.DLL>

namespace Estacionamiento {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;

	/// <summary>
	/// Resumen de Autos
	/// </summary>
	public ref class Autos : public System::Windows::Forms::Form
	{
	public:
		Autos(void)
		{
			InitializeComponent();
			agregarUsuario();
			baja->Enabled = false;
		}

		Autos(int id_usuario)
		{
			this->id_usuario = id_usuario;
			InitializeComponent();
			try {
				conexion = gcnew MySqlConnection(datosConexion);
				conexion->Open();
				this->consulta = "call nombre_usuario(" + this->id_usuario + ")";
				comando = gcnew MySqlCommand(consulta, conexion);
				lector = comando->ExecuteReader();
				while (lector->Read())
					nombre->Text = lector->GetString(0);
				buscar->Enabled = false;
				agregar->Enabled = false;
				baja->Enabled = false;
				salir->Enabled = false;
				conexion->Close();
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Autos()
		{
			if (components)
			{
				delete components;
			}
		}
	private: int id_usuario, idVehiculo;
	private: bool busq = true;
	private: String ^ datosConexion = "datasource=localhost;port=3306;username=root;password=2915;database=estacionamiento";
	private: String ^ consulta;
	private: MySqlConnection ^ conexion;
	private: MySqlCommand ^ comando;
	private: MySqlDataReader ^ lector;
	private: System::Windows::Forms::Label^  titulo;
	protected:
	private: System::Windows::Forms::Label^  propietario;
	private: System::Windows::Forms::Label^  placas;
	private: System::Windows::Forms::Label^  modelo;
	private: System::Windows::Forms::Label^  marca;
	private: System::Windows::Forms::TextBox^  nombre;
	private: System::Windows::Forms::TextBox^  nPlaca;
	private: System::Windows::Forms::TextBox^  model;
	private: System::Windows::Forms::TextBox^  marcaV;
	private: System::Windows::Forms::Label^  noTarjet�n;
	private: System::Windows::Forms::Button^  aceptar;
	private: System::Windows::Forms::Button^  buscar;
	private: System::Windows::Forms::Button^  baja;
	private: System::Windows::Forms::Button^  agregar;
	private: System::Windows::Forms::Button^  salir;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::DataGridView^  tabla;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  tipoBus;






	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Autos::typeid));
			this->titulo = (gcnew System::Windows::Forms::Label());
			this->propietario = (gcnew System::Windows::Forms::Label());
			this->placas = (gcnew System::Windows::Forms::Label());
			this->modelo = (gcnew System::Windows::Forms::Label());
			this->marca = (gcnew System::Windows::Forms::Label());
			this->nombre = (gcnew System::Windows::Forms::TextBox());
			this->nPlaca = (gcnew System::Windows::Forms::TextBox());
			this->model = (gcnew System::Windows::Forms::TextBox());
			this->marcaV = (gcnew System::Windows::Forms::TextBox());
			this->noTarjet�n = (gcnew System::Windows::Forms::Label());
			this->aceptar = (gcnew System::Windows::Forms::Button());
			this->buscar = (gcnew System::Windows::Forms::Button());
			this->baja = (gcnew System::Windows::Forms::Button());
			this->agregar = (gcnew System::Windows::Forms::Button());
			this->salir = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->tabla = (gcnew System::Windows::Forms::DataGridView());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->tipoBus = (gcnew System::Windows::Forms::TextBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tabla))->BeginInit();
			this->SuspendLayout();
			// 
			// titulo
			// 
			this->titulo->AutoSize = true;
			this->titulo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->titulo->Location = System::Drawing::Point(418, 122);
			this->titulo->Name = L"titulo";
			this->titulo->Size = System::Drawing::Size(272, 29);
			this->titulo->TabIndex = 0;
			this->titulo->Text = L"Asignaci�n de vehiculos";
			// 
			// propietario
			// 
			this->propietario->AutoSize = true;
			this->propietario->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->propietario->Location = System::Drawing::Point(569, 290);
			this->propietario->Name = L"propietario";
			this->propietario->Size = System::Drawing::Size(111, 25);
			this->propietario->TabIndex = 1;
			this->propietario->Text = L"Propietario:";
			// 
			// placas
			// 
			this->placas->AutoSize = true;
			this->placas->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->placas->Location = System::Drawing::Point(569, 364);
			this->placas->Name = L"placas";
			this->placas->Size = System::Drawing::Size(77, 25);
			this->placas->TabIndex = 2;
			this->placas->Text = L"Placas:";
			// 
			// modelo
			// 
			this->modelo->AutoSize = true;
			this->modelo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->modelo->Location = System::Drawing::Point(787, 364);
			this->modelo->Name = L"modelo";
			this->modelo->Size = System::Drawing::Size(83, 25);
			this->modelo->TabIndex = 3;
			this->modelo->Text = L"Modelo:";
			// 
			// marca
			// 
			this->marca->AutoSize = true;
			this->marca->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->marca->Location = System::Drawing::Point(571, 441);
			this->marca->Name = L"marca";
			this->marca->Size = System::Drawing::Size(73, 25);
			this->marca->TabIndex = 4;
			this->marca->Text = L"Marca:";
			// 
			// nombre
			// 
			this->nombre->Enabled = false;
			this->nombre->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->nombre->Location = System::Drawing::Point(685, 284);
			this->nombre->Name = L"nombre";
			this->nombre->Size = System::Drawing::Size(368, 30);
			this->nombre->TabIndex = 5;
			// 
			// nPlaca
			// 
			this->nPlaca->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->nPlaca->Location = System::Drawing::Point(653, 360);
			this->nPlaca->MaxLength = 10;
			this->nPlaca->Name = L"nPlaca";
			this->nPlaca->Size = System::Drawing::Size(126, 30);
			this->nPlaca->TabIndex = 6;
			this->nPlaca->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Autos::nPlaca_KeyPress);
			// 
			// model
			// 
			this->model->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->model->Location = System::Drawing::Point(877, 360);
			this->model->MaxLength = 15;
			this->model->Name = L"model";
			this->model->Size = System::Drawing::Size(176, 30);
			this->model->TabIndex = 7;
			this->model->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Autos::model_KeyPress);
			// 
			// marcaV
			// 
			this->marcaV->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->marcaV->Location = System::Drawing::Point(651, 438);
			this->marcaV->MaxLength = 20;
			this->marcaV->Name = L"marcaV";
			this->marcaV->Size = System::Drawing::Size(188, 30);
			this->marcaV->TabIndex = 8;
			this->marcaV->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Autos::marcaV_KeyPress);
			// 
			// noTarjet�n
			// 
			this->noTarjet�n->AutoSize = true;
			this->noTarjet�n->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->noTarjet�n->Location = System::Drawing::Point(870, 205);
			this->noTarjet�n->Name = L"noTarjet�n";
			this->noTarjet�n->Size = System::Drawing::Size(0, 25);
			this->noTarjet�n->TabIndex = 9;
			// 
			// aceptar
			// 
			this->aceptar->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->aceptar->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"aceptar.Image")));
			this->aceptar->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->aceptar->Location = System::Drawing::Point(685, 596);
			this->aceptar->Name = L"aceptar";
			this->aceptar->Size = System::Drawing::Size(135, 63);
			this->aceptar->TabIndex = 10;
			this->aceptar->Text = L"Aceptar";
			this->aceptar->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->aceptar->UseVisualStyleBackColor = true;
			this->aceptar->Click += gcnew System::EventHandler(this, &Autos::aceptar_Click);
			// 
			// buscar
			// 
			this->buscar->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->buscar->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"buscar.Image")));
			this->buscar->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->buscar->Location = System::Drawing::Point(318, 596);
			this->buscar->Name = L"buscar";
			this->buscar->Size = System::Drawing::Size(185, 63);
			this->buscar->TabIndex = 11;
			this->buscar->Text = L"Lista Veh�culos";
			this->buscar->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->buscar->UseVisualStyleBackColor = true;
			this->buscar->Click += gcnew System::EventHandler(this, &Autos::buscar_Click);
			// 
			// baja
			// 
			this->baja->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->baja->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"baja.Image")));
			this->baja->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->baja->Location = System::Drawing::Point(848, 596);
			this->baja->Name = L"baja";
			this->baja->Size = System::Drawing::Size(95, 63);
			this->baja->TabIndex = 12;
			this->baja->Text = L"Baja";
			this->baja->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->baja->UseVisualStyleBackColor = true;
			this->baja->Click += gcnew System::EventHandler(this, &Autos::baja_Click);
			// 
			// agregar
			// 
			this->agregar->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->agregar->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"agregar.Image")));
			this->agregar->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->agregar->Location = System::Drawing::Point(77, 596);
			this->agregar->Name = L"agregar";
			this->agregar->Size = System::Drawing::Size(213, 63);
			this->agregar->TabIndex = 13;
			this->agregar->Text = L"Lista Propietarios";
			this->agregar->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->agregar->UseVisualStyleBackColor = true;
			this->agregar->Click += gcnew System::EventHandler(this, &Autos::agregar_Click);
			// 
			// salir
			// 
			this->salir->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->salir->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"salir.Image")));
			this->salir->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->salir->Location = System::Drawing::Point(968, 596);
			this->salir->Name = L"salir";
			this->salir->Size = System::Drawing::Size(96, 63);
			this->salir->TabIndex = 14;
			this->salir->Text = L"Salir";
			this->salir->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->salir->UseVisualStyleBackColor = true;
			this->salir->Click += gcnew System::EventHandler(this, &Autos::salir_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(166, 9);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(146, 142);
			this->pictureBox1->TabIndex = 15;
			this->pictureBox1->TabStop = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(296, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(610, 36);
			this->label1->TabIndex = 16;
			this->label1->Text = L"INSTITUTO TECNOL�GICO DE OAXACA";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(370, 55);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(420, 29);
			this->label2->TabIndex = 17;
			this->label2->Text = L"DEPARTAMENTO DE VINCULACI�N";
			// 
			// tabla
			// 
			this->tabla->AllowUserToResizeColumns = false;
			this->tabla->AllowUserToResizeRows = false;
			this->tabla->BackgroundColor = System::Drawing::SystemColors::ButtonHighlight;
			this->tabla->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->tabla->EditMode = System::Windows::Forms::DataGridViewEditMode::EditProgrammatically;
			this->tabla->Location = System::Drawing::Point(28, 205);
			this->tabla->MultiSelect = false;
			this->tabla->Name = L"tabla";
			this->tabla->RowTemplate->Height = 28;
			this->tabla->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->tabla->Size = System::Drawing::Size(524, 377);
			this->tabla->TabIndex = 18;
			this->tabla->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Autos::tabla_CellDoubleClick);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(142, 171);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(170, 25);
			this->label3->TabIndex = 20;
			this->label3->Text = L"T�rmino a buscar:";
			// 
			// tipoBus
			// 
			this->tipoBus->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->tipoBus->Location = System::Drawing::Point(332, 169);
			this->tipoBus->MaxLength = 20;
			this->tipoBus->Name = L"tipoBus";
			this->tipoBus->Size = System::Drawing::Size(220, 30);
			this->tipoBus->TabIndex = 21;
			this->tipoBus->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Autos::tipoBus_KeyPress);
			// 
			// Autos
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ButtonHighlight;
			this->ClientSize = System::Drawing::Size(1076, 671);
			this->Controls->Add(this->tipoBus);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->tabla);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->salir);
			this->Controls->Add(this->agregar);
			this->Controls->Add(this->baja);
			this->Controls->Add(this->buscar);
			this->Controls->Add(this->aceptar);
			this->Controls->Add(this->noTarjet�n);
			this->Controls->Add(this->marcaV);
			this->Controls->Add(this->model);
			this->Controls->Add(this->nPlaca);
			this->Controls->Add(this->nombre);
			this->Controls->Add(this->marca);
			this->Controls->Add(this->modelo);
			this->Controls->Add(this->placas);
			this->Controls->Add(this->propietario);
			this->Controls->Add(this->titulo);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"Autos";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Veh�culos";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tabla))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void aceptando() {
		if (nombre->Text == "") {
			MessageBox::Show("El campo propietario debe estar relleno, selecciona alg�n usuario", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			agregarUsuario();
		}
		else if (nPlaca->Text == "") {
			MessageBox::Show("El campo placas debe estar relleno", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			nPlaca->Focus();
		}
		else if (model->Text == "") {
			MessageBox::Show("El campo modelo debe estar relleno", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			model->Focus();
		}
		else if (marcaV->Text == "") {
			MessageBox::Show("El campo marca debe estar relleno", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			marcaV->Focus();
		}
		else {
			conexion = gcnew MySqlConnection(datosConexion);
			conexion->Open();
			if (noTarjet�n->Text == "") {
				try {
					consulta = "call nuevo_vehiculo(" + id_usuario + ",'" + nPlaca->Text + "','" + model->Text + "','" + marcaV->Text + "')";
					comando = gcnew MySqlCommand(consulta, conexion);
					lector = comando->ExecuteReader();
					MessageBox::Show("Se agrego con �xito", "�xito");
					lector->Close();
					conexion->Close();
					salir->Enabled = true;
				}
				catch (Exception^) {
					MessageBox::Show("Ocurri� un problema, favor de contactar con soporte", "ERROR",
						MessageBoxButtons::OK, MessageBoxIcon::Error);
				}
				despuesAgregar();
			}
			else {
				try {
					consulta = "call actualizar_vehiculo(" + idVehiculo + ",'" + nPlaca->Text + "','" + model->Text + "','" + marcaV->Text + "')";
					comando = gcnew MySqlCommand(consulta, conexion);
					lector = comando->ExecuteReader();
					MessageBox::Show("Se modific� con �xito", "�xito");
					lector->Close();
					conexion->Close();
					nombre->Text = "";
				}
				catch (Exception^) {
					MessageBox::Show("Ocurrio un problema, favor de contactar con soporte", "ERROR",
						MessageBoxButtons::OK, MessageBoxIcon::Error);
				}
				despuesAgregar();
			}
		}
		
	}
	private:  System::Void despuesAgregar() {
		nPlaca->Text = "";
		model->Text = "";
		marcaV->Text = "";
		noTarjet�n->Text = "";
		baja->Enabled = false;
		nombre->Text = "";
		nPlaca->Focus();
	}
	private: System::Void aceptar_Click(System::Object^  sender, System::EventArgs^  e) {
		this->aceptando();
	}
	private: System::Void buscar_Click(System::Object^  sender, System::EventArgs^  e) {
		/*AutosBuscar^ x = gcnew AutosBuscar();
		this->Hide();
		if (x->ShowDialog(this) == ::DialogResult::OK) {
			idVehiculo = x->idVehiculo;
			try {
			idVehiculo = System::Int32::Parse(tabla->CurrentRow->Cells[0]->Value->ToString());
				conexion = gcnew MySqlConnection(datosConexion);
				conexion->Open();
				consulta = "call datos_vehiculo(" + idVehiculo + ")";
				comando = gcnew MySqlCommand(consulta, conexion);
				lector = comando->ExecuteReader();
				while (lector->Read()) {
					noTarjet�n->Text = "No. Tarjet�n: "+lector->GetString(0);
					nombre->Text = lector->GetString(1);
					nPlaca->Text = lector->GetString(2);
					model->Text = lector->GetString(3);
					marcaV->Text = lector->GetString(4);
				}
				baja->Enabled = true;
				lector->Close();
				conexion->Close();
			}
			catch (Exception^) {
				MessageBox::Show("Ocurrio un problema, favor de contactar con soporte", "ERROR",
					MessageBoxButtons::OK, MessageBoxIcon::Error);
			}

		}
		this->Show();*/
		try {
			conexion = gcnew MySqlConnection(datosConexion);
			conexion->Open();
			this->consulta = "call lista_vehiculos()";
			MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);
			DataTable^ tabla = gcnew DataTable();
			resultados->Fill(tabla);
			this->tabla->DataSource = tabla;
			delete tabla;
			conexion->Close();
			busq = false;
		}
		catch (Exception^ex) {
			this->Close();
		}
	}
	private: System::Void salir_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	private: System::Void baja_Click(System::Object^  sender, System::EventArgs^  e) {
		try {
			conexion = gcnew MySqlConnection(datosConexion);
			conexion->Open();
			consulta = "call baja_vehiculo(" + idVehiculo + ")";
			comando = gcnew MySqlCommand(consulta, conexion);
			lector = comando->ExecuteReader();
			lector->Close();
			conexion->Close();
			MessageBox::Show("Dado de baja con �xito", "�xito",
				MessageBoxButtons::OK);
		}
		catch (Exception^) {
			MessageBox::Show("Ocurrio un problema, favor de contactar con soporte", "ERROR",
				MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
		finally{
			nPlaca->Text = "";
		model->Text = "";
		marcaV->Text = "";
		nombre->Text = "";
		noTarjet�n->Text = "";
		baja->Enabled = false;
		}
	}
	private: System::Void agregarUsuario() {
		try {
			conexion = gcnew MySqlConnection(datosConexion);
			conexion->Open();
			this->consulta = "call lista_usuarios()";
			MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);
			DataTable^ tabla = gcnew DataTable();
			resultados->Fill(tabla);
			this->tabla->DataSource = tabla;
			delete tabla;
			conexion->Close();
			busq = true;
		}
		catch (Exception^ex) {
			MessageBox::Show("Ocurri� un error, contacta con soporte");
		}
	}
	private: System::Void agregar_Click(System::Object^  sender, System::EventArgs^  e) {
		agregarUsuario();
	}
	private: System::Void nPlaca_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (e->KeyChar == '\r')
			model->Focus();
	}
	private: System::Void model_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (e->KeyChar == '\r')
			marcaV->Focus();
	}
	private: System::Void marcaV_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (e->KeyChar == '\r')
			this->aceptando();
	}
	private: System::Void tipoBus_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (busq) {
			try {
				conexion = gcnew MySqlConnection(datosConexion);
				conexion->Open();
				this->consulta = "call coincidencia_usuarios('" + tipoBus->Text + "')";
				MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);
				DataTable^ tabla = gcnew DataTable();
				resultados->Fill(tabla);
				this->tabla->DataSource = tabla;
				delete tabla;
				conexion->Close();
			}
			catch (Exception^ex) {
				MessageBox::Show("Error");
			}
		}
		else {
			try {
				conexion = gcnew MySqlConnection(datosConexion);
				conexion->Open();
				this->consulta = "call coincidencia_vehiculos('" + tipoBus->Text + "')";
				MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);
				DataTable^ tabla = gcnew DataTable();
				resultados->Fill(tabla);
				this->tabla->DataSource = tabla;
				delete tabla;
				conexion->Close();
			}
			catch (Exception^ex) {
				MessageBox::Show("Error de b�squeda");
			}
		}
	}
private: System::Void tabla_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
	if (busq) {
		id_usuario = System::Int32::Parse(tabla->CurrentRow->Cells[0]->Value->ToString());
		noTarjet�n->Text = "";
		nombre->Text = tabla->CurrentRow->Cells[1]->Value->ToString();
		baja->Enabled = false;
		nPlaca->Text = "";
		model->Text = "";
		marcaV->Text = "";
	}
	else {
		idVehiculo = System::Int32::Parse(tabla->CurrentRow->Cells[0]->Value->ToString());
		noTarjet�n->Text = "No. Tarjet�n: " + tabla->CurrentRow->Cells[0]->Value->ToString();
		nombre->Text = tabla->CurrentRow->Cells[1]->Value->ToString();
		nPlaca->Text = tabla->CurrentRow->Cells[2]->Value->ToString();
		model->Text = tabla->CurrentRow->Cells[3]->Value->ToString();
		marcaV->Text = tabla->CurrentRow->Cells[4]->Value->ToString();
		baja->Enabled=true;
	}
}
};
}
