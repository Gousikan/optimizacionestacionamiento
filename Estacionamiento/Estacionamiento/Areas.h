#pragma once

namespace Estacionamiento {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;


	/// <summary>
	/// Resumen de Areas
	/// </summary>
	public ref class Areas : public System::Windows::Forms::Form
	{
	public:
		Areas(void)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Areas()
		{
			if (components)
			{
				delete components;
			}
		}
	public: String ^ datosConexion = "datasource=localhost;port=3306;username=root;password=2915;database=estacionamiento";

	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::Button^  actualizar;
	private: System::Windows::Forms::Button^  baja;


	private: System::Windows::Forms::Button^  Salir;
	protected:



	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TextBox^  ide;
	private: System::Windows::Forms::TextBox^  descripcion;
	private: System::Windows::Forms::NumericUpDown^  x1;
	private: System::Windows::Forms::NumericUpDown^  x2;
	private: System::Windows::Forms::NumericUpDown^  x3;
	private: System::Windows::Forms::NumericUpDown^  x4;
	private: System::Windows::Forms::NumericUpDown^  y1;
	private: System::Windows::Forms::NumericUpDown^  y2;
	private: System::Windows::Forms::NumericUpDown^  y3;
	private: System::Windows::Forms::NumericUpDown^  y4;

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Areas::typeid));
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->actualizar = (gcnew System::Windows::Forms::Button());
			this->baja = (gcnew System::Windows::Forms::Button());
			this->Salir = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->ide = (gcnew System::Windows::Forms::TextBox());
			this->descripcion = (gcnew System::Windows::Forms::TextBox());
			this->x1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->x2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->x3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->x4 = (gcnew System::Windows::Forms::NumericUpDown());
			this->y1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->y2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->y3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->y4 = (gcnew System::Windows::Forms::NumericUpDown());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y4))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(47, 60);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(583, 161);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Areas::dataGridView1_CellClick);
			// 
			// actualizar
			// 
			this->actualizar->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"actualizar.Image")));
			this->actualizar->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->actualizar->Location = System::Drawing::Point(329, 398);
			this->actualizar->Name = L"actualizar";
			this->actualizar->Size = System::Drawing::Size(75, 52);
			this->actualizar->TabIndex = 1;
			this->actualizar->Text = L"Actualizar";
			this->actualizar->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->actualizar->UseVisualStyleBackColor = true;
			this->actualizar->Click += gcnew System::EventHandler(this, &Areas::actualizar_Click);
			// 
			// baja
			// 
			this->baja->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"baja.Image")));
			this->baja->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->baja->Location = System::Drawing::Point(434, 398);
			this->baja->Name = L"baja";
			this->baja->Size = System::Drawing::Size(75, 51);
			this->baja->TabIndex = 2;
			this->baja->Text = L"Baja";
			this->baja->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->baja->UseVisualStyleBackColor = true;
			this->baja->Click += gcnew System::EventHandler(this, &Areas::baja_Click);
			// 
			// Salir
			// 
			this->Salir->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Salir.Image")));
			this->Salir->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->Salir->Location = System::Drawing::Point(540, 398);
			this->Salir->Name = L"Salir";
			this->Salir->Size = System::Drawing::Size(75, 52);
			this->Salir->TabIndex = 3;
			this->Salir->Text = L"Salir";
			this->Salir->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->Salir->UseVisualStyleBackColor = true;
			this->Salir->Click += gcnew System::EventHandler(this, &Areas::Salir_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(38, 286);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(18, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"x1";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(38, 328);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(18, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"x2";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(38, 373);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(18, 13);
			this->label3->TabIndex = 6;
			this->label3->Text = L"x3";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(38, 413);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(18, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"x4";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(169, 285);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(18, 13);
			this->label5->TabIndex = 8;
			this->label5->Text = L"y1";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(172, 328);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(18, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"y2";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(172, 372);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(18, 13);
			this->label7->TabIndex = 10;
			this->label7->Text = L"y3";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(172, 413);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(18, 13);
			this->label8->TabIndex = 11;
			this->label8->Text = L"y4";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(326, 286);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(43, 13);
			this->label9->TabIndex = 12;
			this->label9->Text = L"Id_area";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(320, 343);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(63, 13);
			this->label10->TabIndex = 13;
			this->label10->Text = L"Descripcion";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(144, 17);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(486, 20);
			this->textBox1->TabIndex = 14;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Areas::textBox1_KeyPress);
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(44, 20);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(95, 13);
			this->label11->TabIndex = 15;
			this->label11->Text = L"Ingresar b�squeda";
			// 
			// ide
			// 
			this->ide->Enabled = false;
			this->ide->Location = System::Drawing::Point(392, 286);
			this->ide->Name = L"ide";
			this->ide->Size = System::Drawing::Size(238, 20);
			this->ide->TabIndex = 16;
			// 
			// descripcion
			// 
			this->descripcion->Location = System::Drawing::Point(392, 341);
			this->descripcion->MaxLength = 25;
			this->descripcion->Name = L"descripcion";
			this->descripcion->Size = System::Drawing::Size(238, 20);
			this->descripcion->TabIndex = 17;
			// 
			// x1
			// 
			this->x1->Location = System::Drawing::Point(80, 285);
			this->x1->Name = L"x1";
			this->x1->Size = System::Drawing::Size(48, 20);
			this->x1->TabIndex = 18;
			// 
			// x2
			// 
			this->x2->Location = System::Drawing::Point(80, 320);
			this->x2->Name = L"x2";
			this->x2->Size = System::Drawing::Size(48, 20);
			this->x2->TabIndex = 19;
			// 
			// x3
			// 
			this->x3->Location = System::Drawing::Point(80, 373);
			this->x3->Name = L"x3";
			this->x3->Size = System::Drawing::Size(48, 20);
			this->x3->TabIndex = 20;
			// 
			// x4
			// 
			this->x4->Location = System::Drawing::Point(80, 413);
			this->x4->Name = L"x4";
			this->x4->Size = System::Drawing::Size(48, 20);
			this->x4->TabIndex = 21;
			// 
			// y1
			// 
			this->y1->Location = System::Drawing::Point(213, 284);
			this->y1->Name = L"y1";
			this->y1->Size = System::Drawing::Size(57, 20);
			this->y1->TabIndex = 22;
			// 
			// y2
			// 
			this->y2->Location = System::Drawing::Point(213, 328);
			this->y2->Name = L"y2";
			this->y2->Size = System::Drawing::Size(57, 20);
			this->y2->TabIndex = 23;
			// 
			// y3
			// 
			this->y3->Location = System::Drawing::Point(213, 370);
			this->y3->Name = L"y3";
			this->y3->Size = System::Drawing::Size(57, 20);
			this->y3->TabIndex = 24;
			// 
			// y4
			// 
			this->y4->Location = System::Drawing::Point(213, 411);
			this->y4->Name = L"y4";
			this->y4->Size = System::Drawing::Size(57, 20);
			this->y4->TabIndex = 25;
			// 
			// Areas
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(670, 498);
			this->Controls->Add(this->y4);
			this->Controls->Add(this->y3);
			this->Controls->Add(this->y2);
			this->Controls->Add(this->y1);
			this->Controls->Add(this->x4);
			this->Controls->Add(this->x3);
			this->Controls->Add(this->x2);
			this->Controls->Add(this->x1);
			this->Controls->Add(this->descripcion);
			this->Controls->Add(this->ide);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->Salir);
			this->Controls->Add(this->baja);
			this->Controls->Add(this->actualizar);
			this->Controls->Add(this->dataGridView1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximumSize = System::Drawing::Size(686, 537);
			this->MinimumSize = System::Drawing::Size(686, 537);
			this->Name = L"Areas";
			this->Load += gcnew System::EventHandler(this, &Areas::Areas_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->y4))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Areas_Load(System::Object^  sender, System::EventArgs^  e) {

		MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
		String^ consulta = "call buscar_todo_area()";

		DataTable^ tabla;//crea una tabla donde se agregaran los resultados
						 //Valido que todos los campos del formulario esten rellenos y no esten vacios
						 //Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
		try {
			conexion->Open();
			MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);	//guarda los resultados del procedimineto			   

			try {
				tabla = gcnew DataTable();
				resultados->Fill(tabla);
				dataGridView1->DataSource = tabla;// guarda en la propiedad de data grid view1, data source laos resultados de la tabla
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}


		}
		catch (Exception^ex) {
			MessageBox::Show(ex->Message);
			//fin m�todo
		}
	}

			 void mostrartodo() {
				 MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
				 String^ consulta = "call buscar_todo_area()";

				 DataTable^ tabla;//crea una tabla donde se agregaran los resultados
								  //Valido que todos los campos del formulario esten rellenos y no esten vacios
								  //Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
				 try {
					 conexion->Open();
					 MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);	//guarda los resultados del procedimineto			   

					 try {
						 tabla = gcnew DataTable();
						 resultados->Fill(tabla);
						 dataGridView1->DataSource = tabla;// guarda en la propiedad de data grid view1, data source laos resultados de la tabla
					 }
					 catch (Exception^ex) {
						 MessageBox::Show(ex->Message);
					 }


				 }
				 catch (Exception^ex) {
					 MessageBox::Show(ex->Message);
					 //fin m�todo
				 }
			 }
private: System::Void actualizar_Click(System::Object^  sender, System::EventArgs^  e) {
	//borrar_usuario
	MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
	String^ consulta = "call actuali_area(" + ide->Text + ",'" + descripcion->Text + "','" + x1->Text + "','" + y1->Text + "','" + x2->Text + "','" + y2->Text + "','" + x3->Text + "','" + y3->Text + "','" + x4->Text + "','" + y4->Text + "')";


	if (ide->Text != "") {
		//Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
		try {
			conexion->Open();
			MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
			MySqlDataReader^ lector;
		

			try {
				lector = comando->ExecuteReader();
				mostrartodo();
				MessageBox::Show("Datos modificados", "Act",
					MessageBoxButtons::OK, MessageBoxIcon::Asterisk);
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}


		}
		catch (Exception^ex) {
			MessageBox::Show(ex->Message);
		}

	}
}
private: System::Void textBox1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
	MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
	String^ consulta = "call area_coincidencia('" + textBox1->Text + "')";
	DataTable^ tabla;//crea una tabla donde se agregaran los resultados
					 //Valido que todos los campos del formulario esten rellenos y no esten vacios
					 //Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
	try {
		conexion->Open();
		MySqlDataAdapter^ resultados = gcnew MySqlDataAdapter(consulta, conexion);	//guarda los resultados del procedimineto			   

		try {
			tabla = gcnew DataTable();
			resultados->Fill(tabla);
			dataGridView1->DataSource = tabla;// guarda en la propiedad de data grid view1, data source laos resultados de la tabla
		}
		catch (Exception^ex) {
			MessageBox::Show(ex->Message);
		}


	}
	catch (Exception^ex) {
		MessageBox::Show(ex->Message);
		//fin m�todo
	}
}
private: System::Void dataGridView1_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

	ide->Text = dataGridView1->CurrentRow->Cells[0]->Value->ToString();
	descripcion->Text = dataGridView1->CurrentRow->Cells[1]->Value->ToString();
	x1->Text = dataGridView1->CurrentRow->Cells[2]->Value->ToString();
	x2->Text = dataGridView1->CurrentRow->Cells[3]->Value->ToString();
	x3->Text = dataGridView1->CurrentRow->Cells[4]->Value->ToString();
	x4->Text = dataGridView1->CurrentRow->Cells[5]->Value->ToString();
	y1->Text = dataGridView1->CurrentRow->Cells[6]->Value->ToString();
	y2->Text = dataGridView1->CurrentRow->Cells[7]->Value->ToString();
	y3->Text = dataGridView1->CurrentRow->Cells[8]->Value->ToString();
	y4->Text = dataGridView1->CurrentRow->Cells[9]->Value->ToString();

}
private: System::Void baja_Click(System::Object^  sender, System::EventArgs^  e) {

	//borrar_usuario
	MySqlConnection ^ conexion = gcnew MySqlConnection(datosConexion);
	String^ consulta = "call baja_area('" + ide->Text + "')";


	if (ide->Text != "") {
		//Metodo que ocupa para poder guardar los datos en la base en este caso el usuario
		try {
			conexion->Open();
			MySqlCommand^ comando = gcnew MySqlCommand(consulta, conexion);
			MySqlDataReader^ lector;

			try {
				lector = comando->ExecuteReader();

				//limpiar
				ide->Clear();
				descripcion->Clear();
				x1->ResetText();
				x2->ResetText();
				x3->ResetText();
				x4->ResetText();

				y1->ResetText();
				y2->ResetText();
				y3->ResetText();
				y4->ResetText();

				mostrartodo();
				MessageBox::Show("Area dado de baja", "Baja",
					MessageBoxButtons::OK, MessageBoxIcon::Hand);
			}
			catch (Exception^ex) {
				MessageBox::Show(ex->Message);
			}
		}
		catch (Exception^ex) {
			MessageBox::Show(ex->Message);
		}

	}
}
private: System::Void Salir_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
};
}
