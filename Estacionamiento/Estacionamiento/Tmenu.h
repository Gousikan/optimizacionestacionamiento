#pragma once
#include "Admin.h"
#include "Autos.h"
#include "Areas.h"
#include <vector>
#include<time.h>

//

#include <conio.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>



namespace Estacionamiento {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;

	/// <summary>
	/// Resumen de Tmenu
	/// </summary>
	public ref class Tmenu : public System::Windows::Forms::Form
	{
	public:
		Tmenu(void)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~Tmenu()
		{
			if (components)
			{
				delete components;
			}
		}

		int f, c;

	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;

	private: System::Windows::Forms::Button^  button5;

	private: System::Windows::Forms::Button^  vehiculos;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;

	private: System::Windows::Forms::Button^  generar;

	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Tmenu::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->vehiculos = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->generar = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(316, 11);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 17);
			this->label1->TabIndex = 0;
			this->label1->Click += gcnew System::EventHandler(this, &Tmenu::label1_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(207, 11);
			this->label3->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(470, 62);
			this->label3->TabIndex = 2;
			this->label3->Text = L"SISTEMA DE OPTIMIZACI�N DEL\r\nESTACIONAMIENTO DEL ITO\r\n";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// button1
			// 
			this->button1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button1.Image")));
			this->button1->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button1->Location = System::Drawing::Point(37, 121);
			this->button1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(100, 68);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Usuarios";
			this->button1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Tmenu::button1_Click);
			// 
			// button2
			// 
			this->button2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button2->Location = System::Drawing::Point(37, 298);
			this->button2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(100, 68);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Ver";
			this->button2->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button2->UseVisualStyleBackColor = true;
			// 
			// button3
			// 
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
			this->button3->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button3->Location = System::Drawing::Point(37, 209);
			this->button3->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(100, 68);
			this->button3->TabIndex = 1;
			this->button3->Text = L"�reas";
			this->button3->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Tmenu::button3_Click);
			// 
			// button5
			// 
			this->button5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button5.Image")));
			this->button5->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button5->Location = System::Drawing::Point(685, 389);
			this->button5->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(100, 68);
			this->button5->TabIndex = 4;
			this->button5->Text = L"Salir";
			this->button5->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Tmenu::button5_Click);
			// 
			// vehiculos
			// 
			this->vehiculos->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"vehiculos.Image")));
			this->vehiculos->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->vehiculos->Location = System::Drawing::Point(37, 389);
			this->vehiculos->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->vehiculos->Name = L"vehiculos";
			this->vehiculos->Size = System::Drawing::Size(100, 68);
			this->vehiculos->TabIndex = 3;
			this->vehiculos->Text = L"Veh�culos";
			this->vehiculos->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->vehiculos->UseVisualStyleBackColor = true;
			this->vehiculos->Click += gcnew System::EventHandler(this, &Tmenu::vehiculos_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(37, 11);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(119, 80);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 10;
			this->pictureBox1->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->Location = System::Drawing::Point(219, 90);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(567, 292);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox2->TabIndex = 11;
			this->pictureBox2->TabStop = false;
			// 
			// generar
			// 
			this->generar->Location = System::Drawing::Point(219, 417);
			this->generar->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->generar->Name = L"generar";
			this->generar->Size = System::Drawing::Size(107, 38);
			this->generar->TabIndex = 13;
			this->generar->Text = L"Generar";
			this->generar->UseVisualStyleBackColor = true;
			this->generar->Click += gcnew System::EventHandler(this, &Tmenu::generar_Click);
			// 
			// Tmenu
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(799, 460);
			this->Controls->Add(this->generar);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->vehiculos);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(817, 507);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(817, 507);
			this->Name = L"Tmenu";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Menu";
			this->Load += gcnew System::EventHandler(this, &Tmenu::Tmenu_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		Admin^ x = gcnew Admin();

	//	x->Show();
		this->Hide();
     	this->Hide(); //OCULTA ESTE FORM
		x->ShowDialog(); //APARECE EL FORM DE MENU
		this->Show(); //AL CERRAR EL MENU SE VUELVE A EJECUTAR LA CONTINUACION DE ESTE CODIGO CON LO CUAL LO VOLVEMOS A MOSTRAR


	}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	Areas^ x = gcnew Areas();

	//	x->Show();
	this->Hide();
	this->Hide(); //OCULTA ESTE FORM
	x->ShowDialog(); //APARECE EL FORM DE MENU
	this->Show(); //AL CERRAR EL MENU SE VUELVE A EJECUTAR LA CONTINUACION DE ESTE CODIGO CON LO CUAL LO VOLVEMOS A MOSTRAR


}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close(); //AQUI ES EL BOTON SALIR, CIERRA ESTE FORM Y CONTINUA CON EL CODIGO PENDIENTE DE LOGIN
}
private: System::Void Tmenu_Load(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void vehiculos_Click(System::Object^  sender, System::EventArgs^  e) {
	Autos^ aut = gcnew Autos();
	this->Hide();
	aut->ShowDialog();
	this->Show();
}

private: System::Double funcion_evaluacion(vector<vector<vector<int>>> individuo, double largo, double ancho) {
	//Dimension del cajon estatico de 2 * 5 m, lo cual corresponde a su area
	double areaTotal = largo * ancho;
	//Debemos saber si tiene salida o no y en base a eso sumar su area tambien de la salida
	double largo_salida = 0;
	double ancho_salida = 0;
	bool salida = false;
	for (int i = 0; i < individuo.size(); i++) {
		int dif = individuo[i][0][0] - individuo[i][1][0]; //Obtenemos la diferencia entre coordenadas x, para determinar su distancia, en caso sea mayor a las dimensiones del cajon, se considerar� salida
		int dif2 = individuo[i][0][1] - individuo[i][1][1]; //Obtenemos diferencia entre coordenadas y de cada cajon
		if (dif > 5 || dif < -5 || dif2>5 || dif2 < -5) {
			largo_salida = dif;
			ancho_salida = dif2;
			salida = true;
		}
	}
	double desperdicio;
	if (salida) { //Si hay salida entonces sumamos area de cajones mas area de salida y se resta al area total
		desperdicio = areaTotal - (individuo.size() - 1) * (5 * 2) - (largo_salida * ancho_salida);
		//ahora se calculara el porcentaje de desperdicio en relacion al area total
		return desperdicio * 1 / areaTotal;
	}
	else {
		desperdicio = areaTotal - (individuo.size() * 5 * 2);
		return -1 * (desperdicio * 1 / areaTotal);
	}
}

private: System::Int16 haySalida(vector<vector<vector<int>>> individuo) {
	int dif,dif2;
	for (int i = 0; i < individuo.size(); i++) {
		dif = individuo[i][1][0] - individuo[i][0][0]; //Obtenemos la diferencia entre coordenadas x, para determinar su distancia, en caso sea mayor a las dimensiones del cajon, se considerar� salida
		dif2 = individuo[i][1][1] - individuo[i][0][1]; //Obtenemos diferencia entre coordenadas y de cada cajon
		MessageBox::Show("dif: "+dif+" dif2: "+dif2);

		if (dif > 5 || dif < -5 || dif2>5 || dif2 < -5) {
			return individuo[i][1][1];
		}
	}
	return 0;
}


private: vector<vector<vector<int>>> algoritmo_genetico(double largo, double ancho) {
	int cont = 1;
	int numGeneraciones = 10;

	vector<vector<vector<vector<int>>>> poblacion = metodo(largo, ancho);//crear poblacion incial , funcion de alberto, aqui meter el metodo

	vector<double> valor_evaluacion;
	bool terminado = false;
	int posPadre, posMadre;
	for (int i = 0; i < poblacion.size(); i++) //se computa la evaluacion para cada individuo y se guarda su valor obtenido
		valor_evaluacion.push_back(funcion_evaluacion(poblacion[i], largo, ancho));
	
	while (!terminado) {
		for (int i = 0; i < poblacion.size() / 2; i++) {
			if (cont != numGeneraciones) {
				int valor = 0; //en caso que sean padres que pueda cruzarse, existira una salida al menos
				while (valor == 0) {
					MessageBox::Show("entro a ver padres validos");
					posPadre = rand() % poblacion.size(); //Elige padre al azar
					posMadre = rand() % poblacion.size(); //Elige madre al azar
					valor = haySalida(poblacion[posPadre]);
					if (valor == 0)
						valor = haySalida(poblacion[posMadre]);
				}
				MessageBox::Show("tiene padre validos");
				vector<vector<vector<int>>> hijo;
				vector<vector<vector<int>>> hija;
				for (int j = 0; j < poblacion[posPadre].size(); j++) {
					if (poblacion[posPadre][j][1][1] <= valor)
						hijo.push_back(poblacion[posPadre][j]);
					if (poblacion[posPadre][j][0][1] > valor)
						hija.push_back(poblacion[posPadre][j]);
				}
				MessageBox::Show("creo hijos");
				for (int j = 0; j < poblacion[posMadre].size(); j++) {
					if (poblacion[posMadre][j][1][1] <= valor)
						hija.push_back(poblacion[posMadre][j]);
					if (poblacion[posMadre][j][0][1] > valor)
						hijo.push_back(poblacion[posMadre][j]);
				}
				MessageBox::Show("muto");
				double evaluacion1 = funcion_evaluacion(hijo, largo, ancho);
				double evaluacion2 = funcion_evaluacion(hija, largo, ancho);
				if (valor_evaluacion[posPadre] > evaluacion1 && hijo.size() > poblacion[posPadre].size()) {
					poblacion[posPadre] = hijo;
					valor_evaluacion[posPadre] = evaluacion1;
				}
				if (valor_evaluacion[posMadre] > evaluacion2 && hija.size() > poblacion[posMadre].size()) {
					poblacion[posMadre] = hija;
					valor_evaluacion[posMadre] = evaluacion2;
				}
				MessageBox::Show("agrego hijos");
				cont++;
			}
			else {
				terminado = true;
			}
		}
	}
	int valPeque�o = 1;
	int posPeque�o;
	for (int x = 0; x < valor_evaluacion.size(); x++) 
		if (valor_evaluacion[x] > 0) 
			if (valor_evaluacion[x] < valPeque�o) {
				valPeque�o = valor_evaluacion[x];
				posPeque�o = x;
			}
	return poblacion[posPeque�o];
	
}
		 //---------------------------------------------------------------------------------------------------------------------------------------------------------
		 bool lugar(int x, int  y, vector<vector<int>> aux) {
			 for (int i = x; i < x + 2; i++) {
				 for (int j = y; j <y + 5; j++) {
					 if (aux[i][j] == 1 || aux[i][j] == 5 || aux[i][j] >= c - 3 || aux[i][j] >= f - 6)
					 {
						 return true;
					 }
				 }
			 }
			 return false;
		 }


		 int random() {
			 srand(time(0));
			 int a = 1 + (rand() % (f - 5));
			 return a;
		 }

		 int random1() {

			 int a = (rand() % (c - 5));
			 return a;
		 }


		 int random2() {

			 int a = (rand() % (f - 2));
			 return a;
		 }

		 vector<vector<int>> generarsalida(vector<vector<int>> aux, int a) { //necesita devolver aux
			 
			 for (int i = a; i < a + 5; i++) {
				 for (int j = 0; j < c; j++) {
					 aux[i][j] = 1;
				 }
			 }
			 return aux;
		 }

		 void imprimir(vector<vector<int>>aux) {
			 for (int i = 0; i < f; i++) {
				 for (int j = 0; j < c; j++) {
					 printf("%d ", aux[i][j]);
				 }
				 printf("\n");
			 }
		 }

		 vector<vector<vector<vector<int>>>> metodo(int a, int b) {
			 vector<vector<int>> aux;
			 for (int i = 0; i < 500; i++) {
				 vector<int> nose;
				 for (int j = 0; j < 500; j++) {
					 nose.push_back(0);
				 }
				 aux.push_back(nose);
			 }

			 f = a;
			 c = b;

			 vector<vector<vector<vector<int>>>> totalpoblacion;

			 for (int p = 0; p < 2; p++) {
				 vector<vector<vector<int>>> individuo;
				 int aprob = random();
				 aux = generarsalida(aux, aprob);
				 vector<int>salida1 = { aprob,0 };
				 vector<int>salida2 = { aprob + 5,a };
				 vector<vector<int>> sali = { salida1,salida2 };
				 individuo.push_back(sali);
				 for (int i = 0; i < 500; i++) {
					 int x1 = random2();
					 int x2 = random1();



					 vector<vector<int>> coordenadas;

					 int x11, y11;
					 if (lugar(x1, x2, aux) == false) {
						 vector <int> unacoordenada;
						 unacoordenada.push_back(x1);
						 unacoordenada.push_back(x2);

						 coordenadas.push_back(unacoordenada);
						 unacoordenada.pop_back();
						 unacoordenada.pop_back();

						 for (int i = x1; i < x1 + 2; i++) {
							 for (int j = x2; j <x2 + 5; j++) {
								 aux[i][j] = 5;
								 x11 = i;
								 y11 = j;

							 }
						 }
						 unacoordenada.push_back(x11);
						 unacoordenada.push_back(y11);

						 coordenadas.push_back(unacoordenada);
					 }


					 if (coordenadas.empty()) {

					 }
					 else {
						 individuo.push_back(coordenadas);
					 }
				 }

				 totalpoblacion.push_back(individuo);
			 }
			 imprimir(aux);

			 return totalpoblacion;
		 }
		 //-----------------------------------------------------------------------------------------------------------------------------------------------------


private: System::Void generar_Click(System::Object^  sender, System::EventArgs^  e) {
	/*AL MOMENTO DE PRESIONAR BOTON DE GENERAR, DEBERA CREAR POBLACI�N INICIAL (METODO DE ALBERTO)
	  Y DESPUES HARA ALGORITMO GEN�TICO */

	//Se debe hacer solicitud a la base y calcular el largo y ancho

	vector<vector<vector<int>>> optimo = algoritmo_genetico(50,30); //para prueba 50,30
	
	vector<vector<vector<int>>> paraGraficar;

	for (int i = 0; i < optimo.size(); i++) {
		vector<int>coord1 = {optimo[i][0][0],optimo[i][0][1]};
		vector<int>coord2 = { optimo[i][0][0],optimo[i][1][1] };
		vector<int>coord3 = { optimo[i][1][0],optimo[i][1][1] };
		vector<int>coord4 = { optimo[i][1][0],optimo[i][0][1] };
		vector<vector<int>> caj = {coord1,coord2,coord3,coord4};
		paraGraficar.push_back(caj);
	}





	//este vector debera ser graficado checa bien la estructura concha
}
};
}
