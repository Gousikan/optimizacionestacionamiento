create database estacionamiento;
-- drop database estacionamiento;
use estacionamiento;

create table fecha_Renovacion(
id_fecha_registro int NOT NULL AUTO_INCREMENT,
fecha_renovacion date NOT NULL,
 PRIMARY KEY (id_fecha_registro));
 
  create table Usuario(
 id_usuario int NOT NULL AUTO_INCREMENT PRIMARY KEY,
 id_fecha_registro int NOT NULL,
 departamento varchar(50) NOT NULL,
 nombre varchar(45) NOT NULL,
 tipo varchar(20) NOT NULL,
 contraseña varchar (40),
 bandera boolean default true,
 foreign key(id_fecha_registro)
references fecha_Renovacion(id_fecha_registro) 
 );
 
  create table Vehiculo(
 id_vehiculo int not null auto_increment primary key,
 id_usuario int not null,
 placas varchar(10) not null,
 modelo varchar(15) not null,
 marca varchar(20) not null,
 banderaV boolean default true,
 foreign key (id_usuario)
 references Usuario (id_usuario)
 );
 
 create table Horario(
id_horario int NOT NULL AUTO_INCREMENT,
id_usuario int NOT NULL,
dia varchar (9) NOT NULL,
hora_entrada int NOT NULL,
hora_salida int NOT NULL,
PRIMARY KEY (id_horario),
FOREIGN KEY (id_usuario) REFERENCES  Usuario (id_usuario));

 
 create table AreaE(
id_area int NOT NULL AUTO_INCREMENT,
descripcion varchar(60),
PRIMARY KEY (id_area),
p_x1 int,
p_y1 int,
p_x2 int,
p_y2 int,
p_x3 int,
p_y3 int,
p_x4 int,
p_y4 int,
bandera boolean default true);

 
 create table Cajon(
id_cajon int  NOT NULL AUTO_INCREMENT,
ancho int,
largo int,
PRIMARY KEY (id_cajon)
);
 
 create table Usuario_Cajon(
id_usuario int NOT NULL,
id_cajon int NOT NULL,
fecha date NOT NULL,
cx1 int,
cx2 int,
cy1 int,
cy2 int,
FOREIGN KEY (id_usuario) REFERENCES  Usuario (id_usuario),
FOREIGN KEY (id_cajon) REFERENCES  Cajon (id_cajon)
);
 
 create table Usuario_Area (
id_area int NOT NULL,
id_usuario int NOT NULL,
cx1 int,
cx2 int,
cy1 int,
cy2 int,
fecha date NOT NULL,
disponibilidad boolean,
FOREIGN KEY (id_usuario) REFERENCES  Usuario (id_usuario),
FOREIGN KEY (id_area) REFERENCES  AreaE (id_area)
 );
 
 
 /* Datos de la fecha renovacion*/


 
 INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (1,"2017/01/01");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (2,"2017/01/02");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (3,"2017/01/03");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (4,"2017/01/04");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (5,"2017/01/05");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (6,"2017/01/06");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (7,"2017/01/07");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (8,"2017/01/08");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (9,"2017/01/09");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (10,"2017/01/10");


INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (11,"2017/01/11");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (12,"2017/01/12");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (13,"2017/01/13");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (14,"2017/01/14");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (15,"2017/01/15");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (16,"2017/01/16");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (17,"2017/01/17");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (18,"2017/01/18");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (19,"2017/01/19");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (20,"2017/01/20");

INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (21,"2017/01/21");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (22,"2017/01/22");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (23,"2017/01/23");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (24,"2017/01/24");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (25,"2017/01/25");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (26,"2017/01/26");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (27,"2017/01/27");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (28,"2017/01/28");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (29,"2017/01/29");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (30,"2017/01/30");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (31,"2017/01/31");

INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (32,"2017/02/01");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (33,"2017/02/02");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (34,"2017/02/03");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (35,"2017/02/04");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (36,"2017/02/05");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (37,"2017/02/06");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (38,"2017/02/07");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (39,"2017/02/08");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (40,"2017/02/09");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (41,"2017/02/10");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (42,"2017/02/11");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (43,"2017/02/12");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (44,"2017/02/13");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (45,"2017/02/14");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (46,"2017/02/15");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (47,"2017/02/16");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (48,"2017/02/17");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (49,"2017/02/18");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (50,"2017/02/19");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (51,"2017/02/20");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (52,"2017/02/21");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (53,"2017/02/22");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (54,"2017/02/23");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (55,"2017/02/24");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (56,"2017/02/25");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (57,"2017/02/26");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (58,"2017/02/27");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (59,"2017/02/28");


INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (60,"2017/03/01");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (61,"2017/03/02");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (62,"2017/03/03");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (63,"2017/03/04");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (64,"2017/03/05");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (65,"2017/03/06");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (66,"2017/03/07");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (67,"2017/03/08");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (68,"2017/03/09");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (69,"2017/03/10");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (70,"2017/03/11");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (71,"2017/03/12");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (72,"2017/03/13");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (73,"2017/03/14");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (74,"2017/03/15");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (75,"2017/03/16");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (76,"2017/03/17");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (77,"2017/03/18");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (78,"2017/03/19");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (79,"2017/03/20");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (80,"2017/03/21");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (81,"2017/03/22");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (82,"2017/03/23");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (83,"2017/03/24");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (84,"2017/03/25");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (85,"2017/03/26");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (86,"2017/03/27");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (87,"2017/03/28");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (88,"2017/03/29");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (89,"2017/03/30");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (90,"2017/03/31");

INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (91,"2017/04/01");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (92,"2017/04/02");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (93,"2017/04/03");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (94,"2017/04/04");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (95,"2017/04/05");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (96,"2017/04/06");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (97,"2017/04/07");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (98,"2017/04/08");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (99,"2017/04/09");
INSERT INTO `fecha_renovacion` (`id_fecha_registro`,`fecha_renovacion`) VALUES (100,"2017/04/10");


/*Datos de usuario*/



INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (2,32," administracion","Josephine","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (3,2,"ing. sistemas computacionales","Mason","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (4,64,"ing. industrial","Robin","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (5,75," ing. quimica","Amaya","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (6,62,"ing. electronica","Leroy","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (7,66,"ing. sistemas computacionales","Quinn","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (8,71,"ing. industrial","Stephen","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (9,87,"ing. electronica","Vanna","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (10,74," ing. quimica","Ulysses","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (11,38,"ing. sistemas computacionales","Regan","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (12,98," ing. quimica","Felix","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (13,5,"ing. civil","Hadley","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (14,90,"ing. civil","Cairo","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (15,16,"ing. electrica","Keely","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (16,17,"ing. civil","Paul","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (17,77,"ing. electronica","Robin","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (18,47,"ing. gestion empresarial","Nayda","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (19,30,"ing. sistemas computacionales","Keelie","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (20,69,"ing. industrial","Walker","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (21,65,"ing. sistemas computacionales","Patience","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (22,44,"ing. industrial","Kylie","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (23,80,"ing. industrial","Prescott","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (24,1," administracion","Robert","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (25,3,"ing. gestion empresarial","Denise","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (26,22,"ing. gestion empresarial","Emily","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (27,90,"ing. electrica","Phillip","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (28,6," ing. quimica","Flavia","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (29,12,"ing. electronica","Rafael","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (30,59," ing. quimica","Kieran","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (31,23,"ing. electrica","Alisa","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (32,57,"ing. electronica","May","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (33,34,"ing. industrial","Martin","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (34,37,"ing. civil","Erin","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (35,22,"ing. sistemas computacionales","Dara","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (36,83,"ing. electrica","Rahim","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (37,80,"ing. sistemas computacionales","Maggy","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (38,20," administracion","Oleg","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (39,19,"ing. gestion empresarial","Vera","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (40,50,"ing. electrica","Giacomo","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (41,47,"ing. sistemas computacionales","Karleigh","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (42,46," administracion","Macaulay","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (43,58," ing. quimica","Marny","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (44,78,"ing. industrial","Blaze","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (45,31," administracion","Jade","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (46,94,"ing. gestion empresarial","Henry","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (47,92,"ing. sistemas computacionales","Axel","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (48,56," ing. quimica","Tashya","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (49,48,"ing. civil","Jameson","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (50,3," ing. quimica","Jordan","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (51,2,"ing. electronica","Caleb","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (52,59,"ing. gestion empresarial","Mari","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (53,35,"ing. civil","Brady","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (54,10,"ing. electrica","Claire","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (55,41,"ing. sistemas computacionales","Anthony","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (56,78,"ing. sistemas computacionales","Kim","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (57,71,"ing. civil","Nehru","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (58,54,"ing. sistemas computacionales","Bruno","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (59,19," ing. quimica","Sade","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (60,3," administracion","Ignacia","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (61,34,"ing. sistemas computacionales","Nathaniel","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (62,39,"ing. civil","Phyllis","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (63,28,"ing. industrial","Lydia","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (64,97,"ing. gestion empresarial","Kibo","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (65,11,"ing. civil","Bo","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (66,96," administracion","Keely","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (67,35,"ing. electronica","Candice","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (68,78," ing. quimica","Samson","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (69,96,"ing. civil","Keaton","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (70,11," ing. quimica","Ivy","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (71,22,"ing. gestion empresarial","Hector","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (72,66,"ing. electronica","Phoebe","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (73,21,"ing. electronica","Graiden","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (74,34,"ing. sistemas computacionales","Deborah","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (75,41,"ing. electronica","Aidan","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (76,96,"ing. gestion empresarial","Griffin","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (77,79,"ing. industrial","Rafael","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (78,59," administracion","Jenette","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (79,64," ing. quimica","Imelda","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (80,81," administracion","Emi","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (81,14,"ing. electronica","Ashton","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (82,32,"ing. gestion empresarial","Guinevere","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (83,29," administracion","Claire","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (84,15,"ing. gestion empresarial","Sylvia","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (85,48,"ing. civil","Melyssa","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (86,47," administracion","Brady","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (87,15,"ing. electronica","Zenia","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (88,38,"ing. industrial","Alden","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (89,19,"ing. industrial","Germaine","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (90,6,"ing. electronica","Ferdinand","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (91,75," administracion","Haviva","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (92,11,"ing. gestion empresarial","Kyle","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (93,96,"ing. electronica","Leonard","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (94,37,"ing. sistemas computacionales","Bethany","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (95,15,"ing. electronica","Calista","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (96,43,"ing. electronica","Cailin","general","","0");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (97,96,"ing. sistemas computacionales","Hollee","general","","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (98,67,"ing. sistemas computacionales","Beto","administrador","1234","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (99,24,"ing. sistemas computacionales","Leon","administrador","1234","1");
INSERT INTO `usuario` (`id_usuario`,`id_fecha_registro`,`departamento`,`nombre`,`tipo`,`contraseña`,`bandera`) VALUES (100,17,"ing. sistemas computacionales","ita","administrador","1234","1");
