use estacionamiento;

drop procedure if exists nombre_usuario;
drop procedure if exists nuevo_vehiculo;
drop procedure if exists lista_vehiculos;
drop procedure if exists datos_vehiculo;
drop procedure if exists actualizar_vehiculo;
drop procedure if exists coincidencia_vehiculos;
drop procedure if exists baja_vehiculo;
drop procedure if exists lista_usuarios;
drop procedure if exists coincidencia_usuarios;
drop procedure if exists idUsuario;

delimiter //
create procedure nombre_usuario(in id int)
begin
select nombre from Usuario where id_usuario = id;
end//

create procedure nuevo_vehiculo (in id_user int, in pl varchar(10), in md varchar(15), in mar varchar(20))
begin 
insert into Vehiculo (id_usuario,placas,modelo,marca) values (id_user,pl,md,mar);
end// 

create procedure lista_vehiculos ()
begin
select id_vehiculo as TARJETON, nombre as NOMBRE, placas as PLACAS, modelo as MODELO, marca as MARCA 
from Vehiculo as a inner join Usuario as b 
on a.id_usuario = b.id_usuario where banderaV = true;
end//

create procedure datos_vehiculo(in id_vehi int)
begin
select id_vehiculo as TARJETON, nombre as NOMBRE, placas as PLACAS, modelo as MODELO, marca as MARCA 
from Vehiculo as a inner join Usuario as b 
on a.id_usuario = b.id_usuario where id_vehiculo = id_vehi;
end//

create procedure actualizar_vehiculo(in id_vehi int, in pl varchar(10), in md varchar(15), in mar varchar(20))
begin
update vehiculo set placas = pl, modelo = md, marca = mar where id_vehiculo = id_vehi;
end//

create procedure coincidencia_vehiculos(in valor varchar(10))
begin
set valor = CONCAT('%',valor,'%');
select id_vehiculo as TARJETON, nombre as NOMBRE, placas as PLACAS, modelo as MODELO, marca as MARCA 
from Vehiculo as a inner join Usuario as b 
on a.id_usuario = b.id_usuario 
where (nombre like valor
or placas like valor
or modelo like valor
or marca like valor) and banderaV = true;
end//

create procedure baja_vehiculo(in id int)
begin
update vehiculo set banderaV = false where id_vehiculo = id;
end//

create procedure lista_usuarios()
begin 
select id_usuario as 'No', nombre as NOMBRE 
FROM usuario where bandera = true;
end//

create procedure coincidencia_usuarios(in valor varchar(10))
begin
set valor = CONCAT('%',valor,'%');
select id_usuario as 'No', nombre as NOMBRE from usuario where nombre like valor and bandera = true;
end//

create procedure idUsuario()
begin
select * from usuario order by id_usuario desc limit 1;
end//
delimiter ;