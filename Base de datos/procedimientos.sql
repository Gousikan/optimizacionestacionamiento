use estacionamiento;

drop procedure if exists nueva_renovacion;
drop procedure if exists inicio_sesion;
drop procedure if exists nuevo_usuario;
drop procedure if exists todos_los_horarios;
drop procedure if exists nuevo_horario;
drop procedure if exists buscar_usuarios_coincidencia;
drop procedure if exists buscar_usuario;
drop procedure if exists borrar_usuario;
drop procedure if exists actualizar_usuario;
drop procedure if exists todos;


delimiter //
create procedure nueva_renovacion (in fecha date)
begin 
insert into fecha_Renovacion (fecha_renovacion) values (fecha);
end// 

create procedure nuevo_usuario(in nom varchar(45), in depto varchar(50), in tip varchar(20),in pass varchar(40),in fecha date)
begin 
declare cont int;
declare idFecha int;
set cont = (select count(id_fecha_registro) from fecha_Renovacion where fecha_renovacion = fecha);
IF cont = 1 then
	set idFecha = (select id_fecha_registro from fecha_Renovacion where fecha_renovacion = fecha);
	insert into Usuario(nombre,departamento,tipo,contraseña,id_fecha_registro) values (nom,depto,tip,pass,idFecha);
else 
	call nueva_renovacion(fecha);
	set idFecha = (select id_fecha_registro from fecha_Renovacion where fecha_renovacion = fecha);
	insert into Usuario(nombre,departamento,tipo,contraseña,id_fecha_registro) values (nom,depto,tip,pass,idFecha);
end IF;
end//

create procedure nuevo_horario(in d varchar(9), in hentrada int, in hsalida int, in id_user int)
begin
insert into Horario(id_usuario,dia,hora_entrada,hora_salida) 
	   values (id_user,d,hentrada,hsalida);
end//

create procedure inicio_sesion(in iduser int, in pass varchar(40), in ti varchar(40))
begin
select count(id_usuario) from Usuario where id_usuario=iduser && contraseña = pass && tipo = ti;
end//

create procedure todos_los_horarios()
begin
select * from horario;
end//

create procedure buscar_usuario(in nom varchar(45))
begin
select id_usuario, id_fecha_registro, nombre, departamento, tipo from usuario where nombre = nom and bandera = true;
end//

create procedure buscar_usuario_id(in nom int)
begin
select * from usuario where id_usuario = nom and bandera = true;
end//

create procedure buscar_usuario_todo(in nom int)
begin
select * from usuario where id_usuario = nom and bandera = true;
end//

create procedure buscar_usuarios_coincidencia(in nom varchar(45))
begin
set nom = CONCAT('%',nom,'%');
select id_usuario as Folio, fecha_renovacion as Folio_renovacio, departamento as Departamento, nombre as Nombre,tipo as Tipo, contraseña as Contraseña 
 from usuario as u inner join fecha_renovacion as f
 on u.id_fecha_registro = f.id_fecha_registro where nombre like nom and bandera = true;
/*select  id_usuario as Folio, fecha_renovacion as Fecha_Renovacion, departamento as Depto, nombre as Nombre, tipo as Tipo
FROM usuario as u inner join fecha_renovacion as f 
on u.id_fecha_registro = f.id_fecha_registro 
WHERE nombre LIKE nom and bandera = true;*/
end//


create procedure borrar_usuario(in identificador int)
begin
update usuario set bandera = false where id_usuario = identificador;
end//

create procedure actualizar_usuario(in identificador int, in nom varchar(45), in depto varchar(50), in tip varchar(20), in pass varchar(40))
begin
update usuario set nombre = nom, departamento = depto, tipo = tip, contraseña = pass where id_usuario = identificador;
end//

create procedure buscarid()
begin
select count(*) from usuario;
end//


create procedure todos()
begin
select u.id_usuario as Id, f.fecha_renovacion as Fecha, u.departamento as Departamento, u.nombre as Nombre,
u.tipo as Tipo, u.contraseña as Contraseña from usuario as u
inner join fecha_renovacion as f
 on u.id_fecha_registro = f.id_fecha_registro
 where bandera = 1;
end//

create procedure buscar_todo_area()
begin
select * from areae where bandera = 1;
end//
delimiter ;
