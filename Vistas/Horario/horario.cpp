#include "horario.h"
#include "ui_horario.h"

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "mysql-connector/include/mysql_connection.h"
#include <mysql-connector/include/cppconn/driver.h>
#include <mysql-connector/include/cppconn/resultset.h>
#include <mysql-connector/include/cppconn/statement.h>
#include <mysql-connector/include/cppconn/exception.h>

Horario::Horario(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Horario)
{
    ui->setupUi(this);
}

Horario::~Horario()
{
    delete ui;
}

int main(){
    sql::Driver *driver;
    sql::Connection *connection;
    sql::Statement *statement;
    sql::ResultSet *resultset;

    //conexion al servido SQL
    driver = get_driver_instance();
    connection = driver->connect("tcp://127.0.0.1:3306", "root", "password");

    //seleccion de la base de datos
    connection->setSchema("estacionamiento");

    //consulta de prueba
    statement = connection->createStatement();
    resultset = statement->executeQuery("SELECT nombre, id_usuario FROM Usuario");

    //impresion de la consulta
    while (resultset->next()) {
            std::cout << "Nombre: " << resultset->getString(1);
            std::cout << "\n Id: " << resultset->getString(2);
            std::cout << std::endl;
    }
}


