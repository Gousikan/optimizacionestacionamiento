#ifndef HORARIO_H
#define HORARIO_H

#include <QMainWindow>

namespace Ui {
class Horario;
}

class Horario : public QMainWindow
{
    Q_OBJECT

public:
    explicit Horario(QWidget *parent = 0);
    ~Horario();

private:
    Ui::Horario *ui;
};

#endif // HORARIO_H
